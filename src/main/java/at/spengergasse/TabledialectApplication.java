package at.spengergasse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TabledialectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TabledialectApplication.class, args);
	}
}
